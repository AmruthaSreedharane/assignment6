from flask import *
import os
import time

import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)

app = application

def connect():
	db = os.environ.get("DB")
	print(db)
	username = os.environ.get("USER")
	print(username)
	password = os.environ.get("PASSWORD")
	print(password)
	hostname = os.environ.get("HOST")
	print(hostname)
 
	return db, username, password, hostname

def create_tables():
	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print("CANT CONNECT")
		print(exp)

	cur = cnx.cursor()
	print("IN CREATE TABLES")
    # Check if table exists or not. Create and populate it only if it does not exist.
	table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title Varchar(255), director Varchar(255), actor Varchar(255), release_date Varchar(255), rating DOUBLE, PRIMARY KEY (id), UNIQUE(title))'

   
	try:
		cur.execute(table_ddl)
		cnx.commit()
		cur.execute('SHOW TABLES')

	except mysql.connector.Error as err:
		if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
			print("already exists.")
		else:
			print("CANT CREATE TABLE")
			print(err.msg)



@app.route("/")
def home():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()
	return render_template("index.html")


@app.route("/add_movie", methods=['GET', 'POST'])
def add_movie():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()
	print("IN INSERT")
	
	i_year = request.form['year']
	print("GOT YEAR")
	i_title = request.form['title']
	print("GOT T")

	i_director = request.form['director']
	print("GOT D")

	i_actor = request.form['actor']
	print("GOT A")

	i_release_date = request.form['release_date']
	print("GOT RD")

	i_rating = request.form['rating']
	print("GOT R")


	try:
		add = 'INSERT INTO movies(year, title, director, actor, release_date, rating) VALUES(%(y)s, %(t)s, %(d)s, %(a)s, %(rd)s, %(r)s)'
		

		data = {
		'y': int(i_year),
		't': i_title,
		'd': i_director,
		'a': i_actor,
		'rd': i_release_date,
		'r': i_rating,

		}
		cur.execute(add, data)
		cnx.commit()


		operation_message_insert = ("Movie %s successfully inserted" % i_title)
	except Exception as e:
		operation_message_insert = ("Movie %s could not be inserted - %s" %(i_title, e))

	print("HERE")
	
	return render_template("index.html", message=operation_message_insert)

@app.route("/update_movie", methods = ['POST'])
def update_movie():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()



	i_year = request.form['year']
	print("GOT YEAR")
	i_title = request.form['title']
	print("GOT T")

	i_director = request.form['director']
	print("GOT D")

	i_actor = request.form['actor']
	print("GOT A")

	i_release_date = request.form['release_date']
	print("GOT RD")

	i_rating = request.form['rating']
	print("GOT R")

	if i_year == "" or i_title == "":
		operation_message_update = ("Movie with %s does not exist" % i_title)
		return render_template("index.html", message=operation_message_update)

	add = 'SELECT * From movies WHERE year = %(y)s AND lower(title)=%(t)s'
		


	data = {
	'y': int(i_year),
	't': i_title.lower()

	}
	cur.execute(add, data)

	if len(cur.fetchall()) == 0:
		operation_message_update = ("Movie with %s does not exist" % i_title)
	else:
		try:
			add = 'UPDATE movies SET director=%(d)s, actor=%(a)s, release_date=%(rd)s, rating=%(r)s WHERE year = %(y)s AND lower(title)=%(t)s'
			

			data = {
			'y': int(i_year),
			't': i_title.lower(),
			'd': i_director,
			'a': i_actor,
			'rd': i_release_date,
			'r': i_rating,

			}
			cur.execute(add, data)
			cnx.commit()


			operation_message_update = ("Movie %s successfully updated" % i_title)
		except Exception as e:
			print(e)
			operation_message_update = ("Movie %s could not be updated - %s" %(i_title, e))

		print("HERE")
	
	return render_template("index.html", message=operation_message_update)

@app.route("/delete_movie", methods = ['POST'])
def delete_movie():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	i_title = request.form['delete_title']
	add = 'SELECT * From movies WHERE lower(title)=%(t)s'
		

	data = {
	't': i_title.lower(),

	}
	cur.execute(add, data)

	if len(cur.fetchall()) == 0:
		operation_message_delete = ("Movie with %s does not exist" % i_title)
	else:
		try:
			add = 'DELETE FROM movies WHERE lower(title)=%(t)s'
			

			data = {
			't': i_title.lower(),

			}
			cur.execute(add, data)
			cnx.commit()


			operation_message_delete = ("Movie %s successfully deleted" % i_title)
		except Exception as e:
			print(e)
			operation_message_delete = ("Movie %s could not be deleted - %s" %(i_title, e))
	return render_template("index.html", message=operation_message_delete, scroll='delete')

@app.route("/search_movie", methods = ['POST'])
def search_movie():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	i_actor = request.form['search_actor']
	print(i_actor)
	operation_message_search=""
	try:
		add = 'SELECT title, year, actor From movies WHERE lower(actor)=%(a)s'
			

		data = {
		'a': i_actor.lower(),

		}
		cur.execute(add, data)
		results = cur.fetchall()
		if len(results) == 0:
			operation_message_search = ("No movies found for actor %s" % i_actor)
			return render_template("index.html", message=operation_message_search)



	except Exception as e:
			print(e)
			operation_message_search = ("Cannot search for actor %s - %s" %(i_actor, e))
			return render_template("index.html", message=operation_message_search)


	print(results)
	for i in results:
		operation_message_search += "Title: " + str(i[0])
		operation_message_search += " Year: " + str(i[1])
		operation_message_search += " Actor: " + str(i[2])

		operation_message_search +=  "\n"
	operation_message_search = operation_message_search.replace('\n', '<br>')

	return render_template("index.html", message=operation_message_search)

@app.route("/highest_rating", methods = ['GET', 'POST'])
def highest_rating():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	try:
		sel = 'SELECT title, year, actor, director, rating From movies where rating in (Select max(rating) From movies)'
			


		cur.execute(sel)
		results = cur.fetchall()

		


	except Exception as e:
			print(e)
			return render_template("index.html", message=e)
	print(results)
	operation_message_stats = ""

	for i in results:
		operation_message_stats += "Title: " + str(i[0])
		operation_message_stats += " Year: " + str(i[1])
		operation_message_stats += " Actor: " + str(i[2])
		operation_message_stats += " Director: " + str(i[3])
		operation_message_stats += " Rating: " + str(i[4])

		operation_message_stats +=  "\n"
	operation_message_stats = operation_message_stats.replace('\n', '<br>')



	return render_template("index.html", message=operation_message_stats, scroll='stats')

@app.route("/lowest_rating", methods = ['GET', 'POST'])
def lowest_rating():
	create_tables()

	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print(exp)

	cur = cnx.cursor()

	try:
		sel = 'SELECT title, year, actor, director, rating From movies where rating in (Select min(rating) From movies)'
			


		cur.execute(sel)
		results = cur.fetchall()
		
	except Exception as e:
			print(e)
			return render_template("index.html", message=e)
	operation_message_stats = ""
	print(results)
	for i in results:
		operation_message_stats += "Title: " + str(i[0])
		operation_message_stats += " Year: " + str(i[1])
		operation_message_stats += " Actor: " + str(i[2])
		operation_message_stats += " Director: " + str(i[3])
		operation_message_stats += " Rating: " + str(i[4])

		operation_message_stats +=  "\n"
	operation_message_stats = operation_message_stats.replace('\n', '<br>')

	return render_template("index.html", message=operation_message_stats, scroll='stats')



if __name__ == "__main__":
	cnx = ''
	db, username, password, hostname = connect()
	try:
		cnx = mysql.connector.connect(user=username, password=password,
	                                      host=hostname,
	                                      database=db)
	except Exception as exp:
		print("CANT CONNECT")
		print(exp)

	cur = cnx.cursor()

	create_tables()
	app.run(host='0.0.0.0')






